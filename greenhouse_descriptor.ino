/**
 * @file arduino_greenhouse_descriptor.ino
 * @author  Fabio Proietti
 * @version 0.5
 *
 * @section LICENSE
 * 
 * MIT License
 * 
 * Copyright (c) 2023 Fabio Proietti
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @section DESCRIPTION
 * 
 * This software controls temperature and humidity in greenhouse. 
 * 
 * greenhouse-descriptor allows to control humidity and temperature in
 * a "Do it yourself" ("DIY") greenhouse. 
 * Hardware required:
 * 1 Arduino Uno rev.3
 * 1 DTH Sensor (humidity and temperature sensor)
 * two relays (I used one module with more relays)
 * 1 humidifer
 * 1 fan
 * the button and the buzzer are still optional and under development)
 * to modify ranges of temperatures and humidities, you need to read the code under "humidity controller logic" section
 * 
*/ 
 
#include "DHT.h"
#define DHTPIN 2 // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.
// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22 // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// using DEBUG = 1, blind people can read the output in Arduino IDE 
// in serial monitor windows
#define DEBUG 1   // DEBUG = 1 show output in serial terminal

// Pinout
#define humidifier 9 // relay
#define unused 10
#define unused2 11
#define fan 12 // relay
#define audio_buzzer 5  // not implemented
#define button 3  // not implemented

#define BIGINTERVAL 180000   //  3 min
#define LITTLEINTERVAL 10000 // 10 sec
#define INTERVAL 60000       //  1 min

/*----------- humidity controller logic ---------------*/

// you can edit this ranges (humidity and temperature)
enum temp { t0 = 5, t1 = 15, t2 = 20, t3 = 28 };
enum humi { h0 = 55, h1 = 60, h2 = 65, h3 = 75, h4 = 80 };

//      t > 28° C -> set 75 < h < 80
// 20°< t < 28° C -> set 65 < h < 75
// 15°< t < 20° C -> set 60 < h < 65
//  5°< t < 15° C -> set 55 < h < 60
//      t < 5°  C -> ice warning

float temperature = 0.0;
float humidity = 0.0;

int hum_state = 0; // hum. is stopped;
int fan_state = 0; // fan is stopped;
void setHumidifier(int state, unsigned long millisec);
void setFan(int state, unsigned long millisec);
void beepCounter(); // not implemented
void interruptHandler(); // not implemented
void sensorReading();
void timeslot(); 

DHT dht(DHTPIN, DHTTYPE);

unsigned long temperature_interval = BIGINTERVAL; // 3 minutes (or 10 sec)
unsigned long humidity_interval = BIGINTERVAL;    // 3 minutes
unsigned long interval = INTERVAL;                // 1 minute

/*-----------------------setup-------------------------*/
void setup() {
  if (DEBUG) {
    Serial.println("setup function");
  }
  pinMode(audio_buzzer, OUTPUT);
  Serial.begin(9600);
  pinMode(fan, OUTPUT);
  // pinMode(11, OUTPUT);
  // pinMode(10, OUTPUT);
  pinMode(humidifier, OUTPUT);
  pinMode(button, INPUT);

  digitalWrite(humidifier, HIGH); /* start turn off: low level*/
  // digitalWrite(10, HIGH);
  // digitalWrite(11, HIGH);
  digitalWrite(fan, HIGH); /*start turn off: low level*/
  digitalWrite(audio_buzzer, LOW);
  attachInterrupt(INT1, interruptHandler, RISING); // pin3 = button
  if (DEBUG) {
    Serial.println(F("DHTxx test!"));
  }
  dht.begin();
  setFan(LOW, 3000);        /*turn off fan in 3 sec*/
  setHumidifier(LOW, 3000); /*turn off humidifier in 3 sec*/
}

/*---------------------loop-------------------------*/
void loop() {
  if (DEBUG) {
    Serial.println("loop function");
  }

  sensorReading();

  // beepCounter();
  if (DEBUG) {
    Serial.println("loop sensor reading done");
  }

  // The following two functions are:
  // fan Controller is limited to controlling only temperature and
  // humidity Controller is limited to controlling only humidity
  
  fanController();
  humController();
}

/*------------------functions-----------------------*/

void timeslot() { 
  // the previous controller functions act for limited time interval
  // this time interval is 3 minutes but can be lower in critical humidity condition

  temperature_interval = BIGINTERVAL; // reset to default value (3 minutes)

  if ((temperature > t3 && humidity <= h2) || 
      (temperature > t2 && humidity <= h1) ||
      (temperature > t1 && humidity < h1-5)) {
    // critical humidity condition
    temperature_interval = LITTLEINTERVAL;
  }
  return;
}

void sensorReading() {
  // Wait a few seconds between two measurements.
  delay(3000); 
  humidity = dht.readHumidity();
  delay(2000);
  temperature = dht.readTemperature();
  if (DEBUG) {
    Serial.print(F("Temperature: "));
    Serial.print(temperature);
    Serial.print(F(" Humidity: "));
    Serial.println(humidity);
  }
  timeslot();
}

void fanController() {

  delay(3000);
  if (DEBUG) {
    Serial.println("Starting fan controller");
  }
  unsigned long initialTime = millis();
  while (1)   
  {
    sensorReading();

    if (DEBUG) {
      Serial.print("milliseconds ");
      Serial.println(millis() - initialTime);
      Serial.println(temperature_interval);
    }

    if ((temperature > ((t2+t3)/2)) && ((millis() - initialTime) <= temperature_interval))
    {
      setFan(HIGH, interval);
      delay(3000);
      if (DEBUG) {
        Serial.println("fan controller with T > 24°");
      }
    } else if ((millis() - initialTime) <= temperature_interval) {
      setFan(LOW, interval);
      delay(3000);
      if (DEBUG) {
        Serial.println("fan controller with T < 24°");
      }
    }

    if ((millis() - initialTime) > temperature_interval) {
      // switch off the fan before return
      setFan(LOW, interval);
      delay(3000);
      if (DEBUG) {
        Serial.println("Halting fan controller");
      }
      return;
    }
  }
}

void humController() {
  // I don't mind of temperature.
  // Turn off the fan.
  if (fan_state != 0)
    setFan(LOW, 3000); /* 3 sec */

  delay(2000);
  if (DEBUG) {
    Serial.println("starting humidifier Controller");
  }
  unsigned long initialTime =
      millis(); // recording initial time for 3 minutes

  while (1) {
    delay(2000);
    if (DEBUG) {
      Serial.print("milliseconds ");
      Serial.println(millis() - initialTime);
    }
    sensorReading(); // updating measures every one minute.
    if ((millis() - initialTime) <
        humidity_interval) // max duration: 3 minutes
    {
      if ((temperature > t3) && humidity <= h3) {
        if (DEBUG) {
          Serial.println("range: temp 28° hum 75%");
        }
        setHumidifier(HIGH, interval);
      } else if ((temperature > t3) && humidity >= h4) {
        if (DEBUG) {
          Serial.println("range: temp 28° hum 80%");
        }
        setHumidifier(LOW, interval);
      } else if ((temperature > t2 && temperature <= t3) && humidity <= h2) {
        if (DEBUG) {
          Serial.println("range: temp 20° hum 65%");
        }
        setHumidifier(HIGH, interval);
      } else if ((temperature > t2 && temperature <= t3) && humidity >= h3) {
        if (DEBUG) {
          Serial.println("range: temp 20° hum 75%");
        }
        setHumidifier(LOW, interval);
      } else if ((temperature >= t1 && temperature <= t2) && humidity < h1) {
        if (DEBUG) {
          Serial.println("range: temp 15° hum 60%");
        }
        setHumidifier(HIGH, interval);
      } else if ((temperature >= t1 && temperature <= t2) && humidity > h2) {
        if (DEBUG) {
          Serial.println("range: temp 15° hum 65%");
        }
        setHumidifier(LOW, interval);
      } else if ((temperature < t1) && humidity < h0) {
        if (DEBUG) {
          Serial.println("range: temp 14° hum 55%");
        }
        setHumidifier(HIGH, interval);
      } else if ((temperature < t1) && humidity > h1) {
        if (DEBUG) {
          Serial.println("range: temp 14° hum 60%");
        }
        setHumidifier(LOW, interval);
      } else if ((temperature < t0)) {
        if (DEBUG) {
          Serial.println("range: temp 5° ice warning!");
        }
        setHumidifier(LOW, interval);
      }
    } else // out of time
    {
      // switch off humidifier before return
      if (DEBUG) {
        Serial.println("Halting humidifier controller");
      }
      // setHumidifier(LOW, interval); // don't turn off the humidifier 
      delay(3000);
      return;
    }
  }
}

void setHumidifier(int state, unsigned long millisec) {
  if (hum_state == 0 && state == LOW) {
    delay(millisec);
    if (DEBUG) {
      Serial.println("humidifier_state off -> off");
    }
    return;
  } else if (hum_state == 0 && state == HIGH) {
    hum_state = 1;
    if (DEBUG) {
      Serial.println("humidifier_state off -> on");
    }
    digitalWrite(humidifier, LOW); /*switch on (low level relay configuration)*/
    delay(millisec);
    return;
  } else if (hum_state == 1 && state == HIGH) {
    if (DEBUG) {
      Serial.println("humidifier_state on -> on");
    }
    delay(millisec);
    return;
  } else if (hum_state == 1 && state == LOW) {
    hum_state = 0;
    if (DEBUG) {
      Serial.println("humidifier_state on -> off");
    }
    digitalWrite(humidifier,
                 HIGH); /*switch off (low level relay configuration)*/
    delay(millisec);
    return;
  }
}

void setFan(int state, unsigned long millisec) {
  if (fan_state == 0 && state == LOW) {
    if (DEBUG) {
      Serial.println("fan_state off -> off");
    }
    delay(millisec);
    return;
  } else if (fan_state == 0 && state == HIGH) {
    fan_state = 1;
    if (DEBUG) {
      Serial.println("fan_state off -> on");
    }
    digitalWrite(fan, LOW); /*switch on (low level relay configuration)*/
    delay(millisec);
    return;
  } else if (fan_state == 1 && state == HIGH) {
    if (DEBUG) {
      Serial.println("fan_state on -> on");
    }
    delay(millisec);
    return;
  } else if (fan_state == 1 && state == LOW) {
    fan_state = 0;
    digitalWrite(fan, HIGH); /*switch off (low level relay configuration)*/
    if (DEBUG) {
      Serial.println("fan_state on -> off");
    }
    delay(millisec);
    return;
  }
}

void beepCounter() {
  // not implemented
  noInterrupts();
  sensorReading();
  int temperature10 = round(temperature * 10);
  int humidityRounded = round(humidity);
  int tot_t = round((temperature10 - 150) / 10); // (16°=1 beep)
  int mod_t = ((temperature10 - 150)) % 10;
  int tot_h = (humidityRounded - 40) / 10; // (50%=1 beep), 2, 3, 4, 5
  int mod_h = (humidityRounded - 40) % 10;
  if (DEBUG) {
    Serial.println("counter beep");
    Serial.print(F("Temperature: "));
    Serial.print(temperature); 
    Serial.print(F(" Humidity: "));
    Serial.println(humidity); 
  }
  
  if (tot_t > 0) {
    for (int it = 0; it < tot_t; it++) {
      /* Here is a problem: a relay switching can generate virtual button pressing */
      tone(audio_buzzer, 20, 600);
      delay(900);
    }
  } 

  delay(5000);

  if (mod_t > 0) {
    for (int rt = 0; rt < mod_t; rt++) {
      tone(audio_buzzer, 15, 600);
      delay(900);
    }
  }

  delay(5000);

  if (tot_h > 0) {
    for (int ih = 0; ih < tot_h; ih++) {
      tone(audio_buzzer, 20, 300);
      delay(900);
    }
  } 
  
  delay(5000);

  if (mod_h > 0) {
    for (int rh = 0; rh < mod_h; rh++) {
      tone(audio_buzzer, 15, 300);
      delay(900);
    }
  } 
  
  delay(3000);
  interrupts();
}

void interruptHandler() {
  // not implemented
  noInterrupts();
  delayMicroseconds(100000); // 0.1 sec
  if (digitalRead(button) == HIGH)
    beepCounter();
  else
    interrupts();
  return;
}
