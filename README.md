# arduino_greenhouse_descriptor

## Description

This software controls temperature and humidity in greenhouse.

arduino_greenhouse_descriptor allows to control humidity and temperature in a "Do it yourself" ("DIY") greenhouse.
This version is accessible by blind people using the serial monitor of Arduino IDE.
In future it will be implemented a pressable button to obtain acoustical feedback (an exact number of beeps for temperature and humidity).

## Dependencies

 * arduino dht-sensor-library

## License

This is free software using MIT license

## Hardware
 * 1 Arduino Uno rev.3
 * 1 DTH Sensor (humidity and temperature sensor)
 * two relays (I used one module with four relays)
 * 1 humidifer (with wall socket)
 * 1 fan (with wall socket)
 * the button and the buzzer are still optional and under development

## Configuration

To modify ranges of temperatures and humidities, you need to read the code under "humidity controller logic" section

### Example

```
    /*----------- humidity controller logic ---------------*/
    
    // you can edit this ranges (humidity and temperature)
    enum temp { t0 = 5, t1 = 15, t2 = 20, t3 = 28 };
    enum humi { h0 = 55, h1 = 60, h2 = 65, h3 = 75, h4 = 80 };
    
    //      t > 28° C -> set 75 < h < 80
    // 20°< t < 28° C -> set 65 < h < 75
    // 15°< t < 20° C -> set 60 < h < 65
    //  5°< t < 15° C -> set 55 < h < 60
    //      t < 5°  C -> ice warning
```

## Pinout

 * Pin 9  : relay for humidifier
 * Pin 12 : relay for fan
 * Pin 5  : buzzer not implemented
 * Pin 3  : button not implemented

## Connection diagram

![greenhouse descriptor schema](docs/img/greenhouse-descriptor-schema.jpg)

## Photos

![Arduino in a box](docs/img/arduino_greenhouse_box2.jpg)
